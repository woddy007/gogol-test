import {compose, lifecycle} from 'recompose';
import {Platform, UIManager} from 'react-native';

import AppView from './AppView';
import {connect} from "react-redux";
import {addProduct} from "./cart/cartProduct/CartProductState";
import {setUser} from "./profile/personalArea/PersonalAreaState";

export default compose(
    connect(
        state => ({}),
        dispatch => ({
            addProduct: (product, count) => dispatch(addProduct(product, count)),
            setUser: (user) => dispatch(setUser(user))
        }),
    ),
    lifecycle({
        componentWillMount() {
            if (Platform.OS === 'android') {
                UIManager.getViewManagerConfig('setLayoutAnimationEnabledExperimental') && UIManager.getViewManagerConfig('setLayoutAnimationEnabledExperimental(true)');
            }
        },
    }),
)(AppView);
