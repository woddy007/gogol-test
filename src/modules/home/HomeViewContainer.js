// @flow
import {compose} from 'recompose';
import {connect} from 'react-redux';

import HomeView from './HomeView';

import {addProduct} from '../cart/cartProduct/CartProductState'

export default compose(
    connect(
        state => ({
            cart: state.cart
        }),
        dispatch => ({
            addProduct: (product, count) => dispatch(addProduct(product, count))
        }),
    ),
)(HomeView);
