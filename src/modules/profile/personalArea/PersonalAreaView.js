import React, {Component} from 'react';
import {
    StyleSheet,
    View,
    Text,
    ScrollView,
    TouchableOpacity,
    Image,
    AsyncStorage
} from 'react-native';

const arrowRight = require('../../../../assets/icons/arrow-right.png')

class PersonalArea extends Component {
    constructor(props) {
        super(props);

        this.state = {}
    }

    componentDidMount = () => {}

    userExit = async () => {
        await AsyncStorage.removeItem('token')
        const {navigate} = this.props.navigation
        this.props.userExit()
        this.props.navigation.replace('Init');
    }

    render() {
        const { navigate } = this.props.navigation

        return (
            <View style={styles.page}>
                <TouchableOpacity
                    onPress={() => {navigate('PersonalInformation')}}
                >
                    <View style={[styles.buttonContent, {borderTopWidth: 1, borderStyle: 'solid', borderColor: '#F1F1F1',}]}>
                        <Text style={styles.buttonText}>Личная информация</Text>
                        <Image
                            style={styles.buttonIcon}
                            source={arrowRight}
                        />
                    </View>
                </TouchableOpacity>

                <TouchableOpacity
                    style={styles.button}
                    onPress={() => {navigate('PurchaseHistory')}}
                >
                    <View style={styles.buttonContent}>
                        <Text style={styles.buttonText}>История заказов</Text>
                        <Image
                            style={styles.buttonIcon}
                            source={arrowRight}
                        />
                    </View>
                </TouchableOpacity>

                <TouchableOpacity
                    style={styles.button}
                    onPress={() => {navigate('DiscountCard')}}
                >
                    <View style={styles.buttonContent}>
                        <Text style={styles.buttonText}>Скидочная карта</Text>
                        <Image
                            style={styles.buttonIcon}
                            source={arrowRight}
                        />
                    </View>
                </TouchableOpacity>

                <TouchableOpacity
                    style={styles.button}
                    onPress={() => this.userExit()}
                >
                    <View style={styles.buttonContent}>
                        <Text style={styles.buttonTextExit}>Выйти</Text>
                    </View>
                </TouchableOpacity>
            </View>
        );
    }

    static navigationOptions = ({navigation}) => {
        return {
            headerTitle: 'Личный кабинет',
        };
    };
}

const styles = StyleSheet.create({
    page: {
        flex: 1,
        paddingVertical: 25
    },

    button: {

    },
    buttonContent: {
        padding: 16,
        borderBottomWidth: 1,
        borderStyle: 'solid',
        borderColor: '#F1F1F1',

        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between'
    },
    buttonText: {
        color: '#666666',
        fontSize: 16
    },
    buttonTextExit: {
        color: '#C3D600'
    },
    buttonIcon: {
        width: 15,
        height: 15,
        tintColor: '#C3D600'
    },
})

export default PersonalArea
