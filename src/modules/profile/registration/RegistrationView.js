import React, {Component} from 'react';
import {
    StyleSheet,
    View,
    Text,
    ScrollView,
    TouchableOpacity,
    TextInput
} from 'react-native';
import {TextInputMask} from "react-native-masked-text";
import common from "../../../styles/common";
import axios from "../../../plugins/axios";
import {DropDownHolder} from "../../../components/DropDownAlert";


class Registration extends Component {
    constructor(props) {
        super(props);

        this.state = {
            phone: '',
            name: ''
        }
    }

    componentDidMount = () => {
        this.setState({
            phone: this.props.navigation.state.params.phone
        })
    }

    inRegistration = () => {
        let phone = String(parseInt(this.state.phone.replace(/\D+/g,"")))
        const { navigate } = this.props.navigation

        let body = new FormData()
        body.append(
            'SignupForm[name]', this.state.name
        )
        body.append(
            'SignupForm[phone]', phone
        )

        axios('post', 'api-shop-shopapp/signup', body).then(response => {
            navigate('Verification', {
                phone: phone
            })
        }).catch(error => {
            let errorText = 'Ошибка сервера'

            if (error.response.data.message) {
                errorText = error.response.data.message
            }

            DropDownHolder.dropDown.alertWithType('error', 'Ошибка', errorText);
        })
    }

    render() {
        const { goBack } = this.props.navigation

        return (
            <View style={styles.page}>
                <Text style={styles.title}>Регистрация</Text>
                <View style={styles.form}>
                    <TextInput
                        value={this.state.name}
                        placeholder="Ваше имя"
                        onChangeText={(name) => {this.setState({name})}}
                        style={[common.input, styles.input]}
                    />
                    <TextInputMask
                        type={'custom'}
                        options={{
                            mask: '+7(999)999-99-99',
                        }}
                        value={this.state.phone}
                        style={[common.input, styles.input]}
                        placeholder="Номер телефона"
                        onChangeText={(phone) => {this.setState({phone})}}
                        placeholderTextColor="#D5D5D5"
                        keyboardType="number-pad"
                    />
                    <TouchableOpacity
                        onPress={() => this.inRegistration()}
                    >
                        <View style={[styles.buttonSave, { marginBottom: 10 }]}>
                            <Text style={styles.buttonSaveText}>РЕГИСТРАЦИЯ</Text>
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity
                        onPress={() => { goBack() }}
                    >
                        <View style={[styles.buttonBack]}>
                            <Text style={styles.buttonBackText}>Назад</Text>
                        </View>
                    </TouchableOpacity>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    page: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    },
    title: {
        fontSize: 24,
        color: '#666666',
        marginBottom: 25
    },
    form: {
        width: '80%'
    },
    input: {
        width: '100%',
        marginBottom: 10
    },

    buttonSave: {
        width: '100%',
        padding: 10,
        borderRadius: 3,
        backgroundColor: '#C3D600',
        alignItems: 'center'
    },
    buttonSaveText: {
        color: '#FFFFFF',
        fontSize: 18
    },

    buttonBack: {
        width: '100%',
        padding: 10,
        borderRadius: 3,
        backgroundColor: 'white',
        alignItems: 'center',
        borderStyle: 'solid',
        borderWidth: 1,
        borderColor: '#C3D600'
    },
    buttonBackText: {
        color: '#C3D600',
        fontSize: 18
    },
})

export default Registration
