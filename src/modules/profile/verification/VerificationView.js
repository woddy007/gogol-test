import React, {Component} from 'react';
import {
    StyleSheet,
    View,
    Text,
    ScrollView,
    TouchableOpacity,
    TextInput,
    AsyncStorage
} from 'react-native';
import common from "../../../styles/common";
import {DropDownHolder} from "../../../components/DropDownAlert";
import axios from "../../../plugins/axios";

class Verification extends Component {
    constructor(props) {
        super(props);

        this.state = {
            phone: '',
            verification: ''
        }
    }

    componentDidMount = () => {
        this.setState({
            phone: this.props.navigation.state.params.phone
        })
    }

    setToken = async (token) => {
        try {
            await AsyncStorage.setItem('token', token);
        } catch (error) {}
    }

    inVerification = () => {
        let { phone, verification } = this.state
        const { replace } = this.props.navigation

        if ( verification ){
            console.log('asdasdas')
            let body = new FormData()
            body.append(
                'username', phone
            )
            body.append(
                'password', verification
            )

            axios('post', 'api-shop-shopapp/login', body).then(response => {
                this.setToken(response.data.jwt_token)
                this.props.navigation.replace({
                    routeName: 'Init'
                })
            }).catch(error => {
                console.log('error: ', error.response)

                DropDownHolder.dropDown.alertWithType('error', 'Ошибка', 'Не правильно введен код');
            })
        }else{
            DropDownHolder.dropDown.alertWithType('error', 'Ошибка', 'Заполните все поля');
        }
    }

    render() {
        const { replace, goBack } = this.props.navigation

        return (
            <View style={styles.page}>
                <Text style={styles.title}>Подтверждение</Text>
                <View style={styles.form}>
                    <TextInput
                        value={this.state.verification}
                        style={[common.input, styles.input]}
                        placeholder="Код из СМС"
                        onChangeText={(verification) => {this.setState({verification})}}
                        placeholderTextColor="#D5D5D5"
                    />
                    <TouchableOpacity
                        onPress={() => this.inVerification()}
                    >
                        <View style={[styles.buttonSave, { marginBottom: 10 }]}>
                            <Text style={styles.buttonSaveText}>Войти</Text>
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity
                        onPress={() => { goBack() }}
                    >
                        <View style={[styles.buttonBack]}>
                            <Text style={styles.buttonBackText}>Назад</Text>
                        </View>
                    </TouchableOpacity>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    page: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    },
    title: {
        fontSize: 24,
        color: '#666666',
        marginBottom: 25
    },
    form: {
        width: '80%'
    },
    input: {
        width: '100%',
        marginBottom: 10
    },

    buttonSave: {
        width: '100%',
        padding: 10,
        borderRadius: 3,
        backgroundColor: '#C3D600',
        alignItems: 'center'
    },
    buttonSaveText: {
        color: '#FFFFFF',
        fontSize: 18
    },

    buttonBack: {
        width: '100%',
        padding: 10,
        borderRadius: 3,
        backgroundColor: 'white',
        alignItems: 'center',
        borderStyle: 'solid',
        borderWidth: 1,
        borderColor: '#C3D600'
    },
    buttonBackText: {
        color: '#C3D600',
        fontSize: 18
    },
})

export default Verification
