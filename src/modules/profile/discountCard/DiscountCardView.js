import React, {Component} from 'react';
import {
    StyleSheet,
    View,
    Text,
    ScrollView,
    TouchableOpacity,
    Image
} from 'react-native';
import * as Brightness from 'expo-brightness';
import * as Permissions from 'expo-permissions';

const DiscontCard = require('../../../../assets/images/discont-card.png')

class DiscountCard extends Component {
    constructor(props) {
        super(props);

        this.state = {}
    }

    componentDidMount = async () => {
        const {status} = await Permissions.askAsync(Permissions.SYSTEM_BRIGHTNESS);

        if (status === 'granted') {
            let currentBrighness = await Brightness.getBrightnessAsync()

            this.setState({
                brighness: currentBrighness
            })

            Brightness.setSystemBrightnessAsync(1);
        }
    };
    componentWillUnmount = async () => {
        const {status} = await Permissions.askAsync(Permissions.SYSTEM_BRIGHTNESS);

        if (status === 'granted') {
            Brightness.setSystemBrightnessAsync(this.state.brighness);
        }
    }

    getImageBase64 = () => {
        const base64 = 'data:image/png;base64,'
        return base64 + 'iVBORw0KGgoAAAANSUhEUgAAAMoAAAAeAQMAAABXBBPSAAAABlBMVEX///8AAABVwtN+AAAAAXRSTlMAQObYZgAAACxJREFUKJFj+Mx/mPkwz4c/Nh9sDh8wZj5w/rOBvfGBD0DMMCo1KjUqBZMCAPummfxHmWQ8AAAAAElFTkSuQmCC'
    }

    render() {
        return (
            <View style={styles.page}>
                <Image
                    style={styles.image}
                    resizeMode={'contain'}
                    source={DiscontCard}
                />
                <Image
                    resizeMode="contain"
                    style={styles.barcode}
                    source={{uri: this.getImageBase64()}}
                />
                <Text style={styles.cardNumber}>80000000137569</Text>
            </View>
        );
    }

    static navigationOptions = ({navigation}) => {
        return {
            headerTitle: 'Скидочная карта',
        };
    };
}

const styles = StyleSheet.create({
    page: {
        flex: 1,
        padding: 15,
    },

    image: {
        width: '100%',
        maxHeight: 200
    },
    barcode: {
        width: '100%',
        height: 75,
    },
    cardNumber: {
        fontSize: 16,
        fontWeight: '500'
    }
})

export default DiscountCard
