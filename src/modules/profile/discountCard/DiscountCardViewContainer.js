// @flow
import { compose } from 'recompose';
import { connect } from 'react-redux';

import DiscountCardView from './DiscountCardView';

export default compose(
  connect(
    state => ({}),
    dispatch => ({}),
  ),
)(DiscountCardView);
