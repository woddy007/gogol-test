import React, {Component} from 'react';
import {
    StyleSheet,
    View,
    Text,
    ScrollView,
    TouchableOpacity,
    TextInput
} from 'react-native';
import common from "../../../styles/common";
import {TextInputMask} from "react-native-masked-text";
import {DropDownHolder} from "../../../components/DropDownAlert";
import axios from "../../../plugins/axios";


class Authorization extends Component {
    constructor(props) {
        super(props);

        this.state = {
            phone: ''
        }
    }

    componentDidMount = () => {}

    inLogin = () => {
        let phone = String(parseInt(this.state.phone.replace(/\D+/g,"")))

        if ( phone.length == 11 ){
            const { navigate } = this.props.navigation

            let body = new FormData()
            body.append(
                'PasswordResetRequestForm[phone]', phone
            )

            axios('post', 'api-shop-shopapp/request', body).then(response => {
                console.log('response: ', response)
                navigate({
                    routeName: 'Verification',
                    params: {
                        phone: phone
                    }
                })
            }).catch(error => {
                console.log('error: ', error.response)

                navigate({
                    routeName: 'Registration',
                    params: {
                        phone: this.state.phone
                    }
                })
            })
        }else{
            DropDownHolder.dropDown.alertWithType('error', 'Ошибка', 'Не правильно введен телефон');
        }
    }

    isCheckUser = () => {
        return false
    }

    render() {
        return (
            <View style={styles.page}>
                <Text style={styles.title}>Вход</Text>
                <View style={styles.form}>
                    <TextInputMask
                        type={'custom'}
                        options={{
                            mask: '+7(999)999-99-99',
                        }}
                        value={this.state.phone}
                        style={[common.input, styles.input]}
                        placeholder="Номер телефона"
                        onChangeText={(phone) => {this.setState({phone})}}
                        placeholderTextColor="#D5D5D5"
                        keyboardType="number-pad"
                    />
                    <TouchableOpacity
                        style={styles.button}
                        onPress={() => this.inLogin()}
                    >
                        <View style={[styles.buttonSave]}>
                            <Text style={styles.buttonSaveText}>Войти</Text>
                        </View>
                    </TouchableOpacity>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    page: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    },
    title: {
        fontSize: 24,
        color: '#666666',
        marginBottom: 25
    },
    form: {
        width: '80%'
    },
    input: {
        width: '100%',
        marginBottom: 10
    },

    buttonSave: {
        width: '100%',
        padding: 10,
        borderRadius: 3,
        backgroundColor: '#C3D600',
        alignItems: 'center'
    },
    buttonSaveText: {
        color: '#FFFFFF',
        fontSize: 18
    }
})

export default Authorization
