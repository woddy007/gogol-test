// @flow
import { compose } from 'recompose';
import { connect } from 'react-redux';

import AuthorizationView from './AuthorizationView';

export default compose(
  connect(
    state => ({}),
    dispatch => ({}),
  ),
)(AuthorizationView);
