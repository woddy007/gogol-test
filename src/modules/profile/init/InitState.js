// @flow
type InitStateType = {};

type ActionType = {
  type: string,
  payload?: any,
};

export const initialState: InitStateType = {};

export const ACTION = 'InitState/ACTION';

export function actionCreator(): ActionType {
  return {
    type: ACTION,
  };
}

export default function InitStateReducer(state: InitStateType = initialState, action: ActionType): InitStateType {
  switch (action.type) {
    case ACTION:
      return {
        ...state,
      };
    default:
      return state;
  }
}
