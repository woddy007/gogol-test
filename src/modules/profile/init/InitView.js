import React, {Component} from 'react';
import {
    StyleSheet,
    View,
    Text,
    ScrollView,
    TouchableOpacity,
    ActivityIndicator,
    AsyncStorage
} from 'react-native';
import axios from "../../../plugins/axios";


class Init extends Component {
    constructor(props) {
        super(props);

        this.state = {}
    }

    componentDidMount = () => {
        const {replace} = this.props.navigation

        if (this.props.user.user) {
            replace('PersonalArea')
        } else {
            if (!this.isCheckToken()) {
                replace('Authorization')
            }
        }
    }

    isCheckToken = async () => {
        const {replace} = this.props.navigation
        let token = await AsyncStorage.getItem('token')

        if (token) {
            axios('get', 'api-shop-shopapp/me').then(response => {
                let user = response.data
                this.props.setUser(user)

                replace('PersonalArea')
            }).catch(error => {
                replace('Authorization')
            })
        } else {
            replace('Authorization')
        }

    }

    render() {
        return (
            <View style={{flex: 1, justifyContent: 'center', alignItems: 'center', backgroundColor: '#C3D600'}}>
                <ActivityIndicator
                    animating
                    size="large"
                    color="#39B54A"
                />
            </View>
        );
    }
}

const styles = StyleSheet.create({
    page: {
        flex: 1,
        paddingVertical: 16,
        backgroundColor: '#F3F5F7'
    },
})

export default Init
