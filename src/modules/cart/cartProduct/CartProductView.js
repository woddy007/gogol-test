import React, {Component} from 'react';
import {
    StyleSheet,
    View,
    Text,
    ScrollView,
    TouchableOpacity,
    FlatList
} from 'react-native';
import CartProductItem from "../../../components/CartProductItem";

class CartProduct extends Component {
    constructor(props) {
        super(props);

        this.state = {}
    }

    componentDidMount = () => {
    }
    clearCart = () => {
        this.props.clearCart()
    }
    toOrder = () => {
    }

    getListCart = () => {
        let list = []
        let object = this.props.cart.list

        for (let key in object) {
            let item = object[key]

            list.push(item)
        }

        return list
    }

    changeCount = (id, count) => {
        this.props.changeCountProduct(id, count)
    }
    deleteProduct = (id) => {
        this.props.deleteProduct(id)
    }

    render() {
        const {navigate} = this.props.navigation;

        return (
            <View style={{flex: 1}}>
                {
                    (Object.keys(this.props.cart.list).length > 0) ? <View style={styles.page}>
                        <FlatList
                            data={this.getListCart()}
                            contentContainerStyle={{marginBottom: 30}}
                            showsHorizontalScrollIndicator={false}
                            renderItem={({item, idx}) => (
                                <CartProductItem
                                    key={'cart-' + idx}
                                    item={item}
                                    changeCount={(id, count) => this.changeCount(id, count)}
                                    deleteProduct={(id) => this.deleteProduct(id)}
                                />
                            )}
                            keyExtractor={(item, idx) => `cart ${idx}`}
                        />
                        <View style={styles.contentButtons}>
                            <TouchableOpacity
                                onPress={() => this.clearCart()}
                            >
                                <View style={styles.buttonClearCart}>
                                    <Text style={styles.buttonClearCartText}>Очистить корзину</Text>
                                </View>
                            </TouchableOpacity>
                            <TouchableOpacity
                                onPress={() => {navigate('CartOrder')}}
                            >
                                <View style={styles.buttonOrder}>
                                    <Text style={styles.buttonOrderText}>Перейти к оформлению</Text>
                                </View>
                            </TouchableOpacity>
                        </View>
                    </View> : <View style={styles.nullContent}>
                        <Text style={styles.nullContentTitle}>В вашей корзине пусто</Text>
                        <Text style={styles.nullContentCaption}>Вы пока ничего не добавили в Корзину</Text>
                        <View style={styles.nullButtonContent}>
                            <TouchableOpacity
                                onPress={() => navigate('Home')}
                            >
                                <View style={[styles.buttonOrder, {width: '100%', borderRadius: 3,}]}>
                                    <Text style={styles.buttonOrderText}>НАЧАТЬ ПОКУПКИ</Text>
                                </View>
                            </TouchableOpacity>
                        </View>
                    </View>
                }
            </View>
        );
    }

    static navigationOptions = ({navigation}) => {
        return {
            headerTitle: 'Корзина',
        };
    };
}

const styles = StyleSheet.create({
    page: {
        flex: 1,
    },
    contentButtons: {
        marginTop: 'auto'
    },
    buttonClearCart: {
        padding: 10,
        width: '100%',
        backgroundColor: 'white',
        alignItems: 'center',
        borderWidth: 1,
        borderStyle: 'solid',
        borderColor: '#C3D600'
    },
    buttonClearCartText: {
        color: '#C3D600'
    },

    buttonOrder: {
        backgroundColor: '#C3D600',
        padding: 10,
        width: '100%',
        alignItems: 'center'
    },
    buttonOrderText: {
        color: 'white'
    },

    nullContent: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    },
    nullContentTitle: {
        fontSize: 22,
        color: 'black',
        fontWeight: 'bold',
        marginBottom: 12
    },
    nullContentCaption: {
        fontSize: 14,
        fontWeight: '300',
        color: '#7C7C7C',
        marginBottom: 25
    },
    nullButtonContent: {
        width: '60%'
    }
})

export default CartProduct
