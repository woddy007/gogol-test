const blueVersion = {
    primary: '#8F7E5A',
    primaryLight: 'rgba(143, 126, 110, 0.6)',

    secondary: '#EF5252',
    secondaryLight: '#EF5252',

    grey: '#acacac',
    gray: '#5f5f5f',
    darkGray: '#4d4d4d',
    lightGray: '#9b9b9b',
    white: '#ffffff',
    blue: '#5A81F7',
    bluish: '#F1F1F7',
    black: '#000000',
    green: '#6DD0A3',
    yellow: '#ffc247',
};

export default blueVersion;
