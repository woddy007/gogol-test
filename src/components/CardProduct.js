import React, {Component} from 'react'
import {
    View,
    StyleSheet,
    Image,
    Text,
    TouchableHighlight,
    Vibration,
    TouchableOpacity
} from 'react-native';

const lineProduct = require('../../assets/images/lineImage.png')

class CardProduct extends Component {
    constructor(props) {
        super(props)

        this.state = {
            colorLine: null,
            showFastBuy: false,

            listColors: [
                '#e4231b',
                '#ffde5e',
                '#e0e578',
                '#ff6b00',
                '#9ac73c',
            ],
        }
    }

    componentDidMount = () => {
        this.getLineColor()
    }

    getRandomNumber = (max) => {
        return Math.floor(Math.random() * Math.floor(max))
    }
    getLineColor = () => {
        let indexColor = this.getRandomNumber(4);

        this.setState({
            colorLine: this.state.listColors[indexColor]
        })
    }
    getPositionLine = () => {
        let index = this.props.index

        if ((index % 2) == 0) {
            return {
                left: 0
            }
        } else {
            return {
                right: 0
            }
        }
    }

    clickCard = () => {
        let {navigate} = this.props.props.navigation

        navigate('Product', {
            id: this.props.item.id
        })
    }
    fastBuyOpen = () => {
        this.setState({showFastBuy: true})

        setTimeout(() => {
            this.setState({showFastBuy: false})
        }, 3000)
    }

    fastBuy = () => {
        this.props.fastBuyProduct(this.props.item)
        this.setState({showFastBuy: false})
    }

    render() {
        let product = this.props.item

        return (
            <View style={styles.card}>
                <TouchableOpacity
                    onPress={() => this.clickCard()}
                    onLongPress={() => this.fastBuyOpen()}
                >
                    <View style={[{flex: 1, backgroundColor: 'white'}]}>
                        <View style={styles.imageContent}>
                            <Image
                                resizeMode="contain"
                                style={[styles.imageLine, this.getPositionLine(), {tintColor: this.state.colorLine}]}
                                source={lineProduct}
                            />
                            <Image
                                style={styles.image}
                                resizeMode="contain"
                                source={{uri: product.image}}
                            />
                            {
                                (this.state.showFastBuy)
                                &&
                                <TouchableHighlight
                                    style={styles.cardFastBuy}
                                    onPress={() => this.fastBuy()}
                                >
                                    <Text style={styles.cardFastBuyText}>+</Text>
                                </TouchableHighlight>
                            }
                        </View>
                        <Text style={styles.category}>{this.props.categoryName}</Text>
                        <Text style={styles.name}>{product.name}</Text>
                        <View style={styles.infoLine}>
                            <Text style={styles.count}>1шт</Text>
                            <View style={styles.priceView}>
                                <Text style={styles.priceText}>
                                    {Number(product.price_new)}₽
                                </Text>
                            </View>
                        </View>
                    </View>
                </TouchableOpacity>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    card: {
        width: '50%',
        paddingLeft: 8,
        marginBottom: 25,
    },
    imageContent: {
        height: 140,
        borderRadius: 5,
        padding: 10,
        overflow: 'hidden',
        position: 'relative',
        marginBottom: 11
    },
    image: {
        flex: 1
    },
    imageLine: {
        position: 'absolute',
        width: '160%',
        top: -100
    },
    category: {
        textTransform: 'uppercase',
        color: '#535353',
        fontSize: 16,
        marginBottom: 3
    },
    name: {
        textTransform: 'uppercase',
        fontSize: 18,
        color: '#C3D600',
        marginBottom: 10
    },
    infoLine: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
    },
    count: {
        fontSize: 14,
        fontWeight: '300',
        color: '#666666'
    },
    priceView: {
        paddingVertical: 3,
        paddingHorizontal: 10,
        backgroundColor: '#C3D600',
        borderRadius: 20
    },
    priceText: {
        color: 'white',
        fontWeight: '600',
        fontSize: 16
    },

    cardFastBuy: {
        position: 'absolute',
        width: 60,
        height: 60,
        borderRadius: 100,
        backgroundColor: '#FFFFFF',
        shadowOffset: {width: 4, height: 4},
        shadowColor: 'rgb(0, 0, 0)',
        shadowOpacity: 0.15,
        justifyContent: 'center',
        alignItems: 'center',
        zIndex: 5,

        left: '50%',
        top: '50%',
        transform: [
            {translateX: -20},
            {translateY: -20},
        ],
    },
    cardFastBuyText: {
        color: '#C3D600',
        fontSize: 30
    }
})

export default CardProduct
