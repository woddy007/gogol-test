import React, {Component} from 'react'
import {
    View,
    StyleSheet,
    Image,
    Text, TouchableOpacity
} from 'react-native';
import Swipeable from "react-native-gesture-handler/Swipeable";

const lineProduct = require('../../assets/images/lineImage.png');
const iconDelete = require('../../assets/icons/delete.png');

class CartProductItem extends Component {
    constructor(props) {
        super(props)

        this.state = {
            count: 1,

            timeIntervar: 200,
        }

        this.gradualIncreaseCount = null
        this.gradualDecreaseCount = null

        this.refSwipeable = React.createRef();
    }

    componentDidMount = () => {
        this.setState({
            count: this.props.item.count
        })
    }

    componentDidUpdate = (prevProps, prevState, snapshot) => {
        if ( prevProps.item.count != this.props.item.count ){
            this.setState({
                count: this.props.item.count
            })
        }
    }

    wait = (timeout) => {
        return new Promise(resolve => {
            setTimeout(resolve, timeout);
        });
    }

    addCountProduct = () => {
        this.setState({count: this.state.count + 1})
    }
    downCountProduct = () => {
        if (this.state.count > 1) {
            this.setState({count: this.state.count - 1})
        } else {
            this.gradualDecreaseCountEnd()
        }
    }

    gradualIncreaseCountStart = () => {
        this.addCountProduct()

        this.gradualIncreaseCount = setInterval(() => {
            this.addCountProduct()
        }, this.state.timeIntervar)
    }
    gradualIncreaseCountEnd = () => {
        clearInterval(this.gradualIncreaseCount)

        this.wait(500).then(() => {
            if (this.props.item.count != this.state.count) {
                this.wait(500).then(() => {
                    this.props.changeCount(this.props.item.id, this.state.count)
                })
            }
        })
    }

    gradualDecreaseCountStart = () => {
        this.downCountProduct()

        this.gradualDecreaseCount = setInterval(() => {
            this.downCountProduct()
        }, this.state.timeIntervar)
    }
    gradualDecreaseCountEnd = () => {
        clearInterval(this.gradualDecreaseCount)

        if (this.props.item.count != this.state.count) {
            this.wait(500).then(() => {
                this.props.changeCount(this.props.item.id, this.state.count)
            })
        }
    }

    deleteProduct = () => {
        this.props.deleteProduct(this.props.item.id)

        this.refSwipeable.current.close()
    }

    renderRightActions = () => {
        return (
            <View style={{width: 80, flexDirection: 'row'}}>
                <TouchableOpacity
                    onPress={() => this.deleteProduct()}
                >
                    <View
                        style={[styles.rightAction, {backgroundColor: '#EF5252'}]}
                    >
                        <Image
                            style={{width: 25, height: 25, tintColor: 'white'}}
                            source={iconDelete}
                        />
                    </View>
                </TouchableOpacity>
            </View>
        )
    }

    render() {
        let product = this.props.item

        return (
            <Swipeable
                ref={this.refSwipeable}
                renderRightActions={this.renderRightActions}
                overshootRight={false}
            >
                <View style={styles.card}>
                    <View style={styles.imageContent}>
                        <Image
                            resizeMode="cover"
                            style={styles.imageLine}
                            source={lineProduct}
                        />
                        <Image
                            resizeMode="contain"
                            style={styles.image}
                            source={{uri: product.photos[0]['src']}}
                        />
                    </View>
                    <View style={styles.content}>
                        <Text style={styles.category}>{product.category.name}</Text>
                        <Text style={styles.name}>{product.name}</Text>
                        <View style={styles.lineInfo}>
                            <View style={styles.viewCount}>
                                <TouchableOpacity
                                    onPress={this.downCountProduct}
                                    onPressOut={() => this.gradualDecreaseCountEnd()}
                                    onLongPress={() => this.gradualDecreaseCountStart()}
                                >
                                    <View style={styles.countButton}>
                                        <Text style={styles.countButtonText}>-</Text>
                                    </View>
                                </TouchableOpacity>

                                <Text style={styles.countValue}>{this.state.count}шт</Text>

                                <TouchableOpacity
                                    onPress={this.addCountProduct}
                                    onPressOut={() => this.gradualIncreaseCountEnd()}
                                    onLongPress={() => this.gradualIncreaseCountStart()}
                                >
                                    <View style={[styles.countButton, {backgroundColor: '#C3D600'}]}>
                                        <Text style={[styles.countButtonText, {color: 'white'}]}>+</Text>
                                    </View>
                                </TouchableOpacity>
                            </View>
                            <Text style={styles.price}>
                                {Number(product.price) * this.state.count}₽
                            </Text>
                        </View>
                    </View>
                </View>
            </Swipeable>
        )
    }
}

const styles = StyleSheet.create({
    card: {
        flexDirection: 'row',
        width: '100%',
        padding: 15,
        borderBottomWidth: 1,
        borderColor: '#F1F1F1',
        borderStyle: 'solid',
        backgroundColor: 'white'
    },
    imageContent: {
        width: 110,
        height: 110,
        backgroundColor: '#f7f6f2',
        borderRadius: 5,
        padding: 10,
        overflow: 'hidden',
        position: 'relative'
    },
    imageLine: {
        tintColor: '#C3D600',
        position: 'absolute',
        width: 110,
        top: 0,
        left: 0,
        zIndex: -1
    },
    image: {
        flex: 1
    },
    content: {
        paddingLeft: 10,
        flex: 1
    },
    category: {
        textTransform: 'uppercase',
        color: '#666666',
        fontSize: 16,
        marginBottom: 6,
        fontWeight: '600'
    },
    name: {
        textTransform: 'uppercase',
        color: '#C3D600',
        fontSize: 16,
        fontWeight: '600',
        marginBottom: 15
    },
    lineInfo: {
        marginTop: 'auto',
        flexDirection: 'row',
        alignItems: 'center'
    },
    viewCount: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    countValue: {
        fontWeight: '300',
        color: '#666666',
        fontSize: 16,
        marginHorizontal: 5
    },
    countButton: {
        width: 25,
        height: 25,
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 100,
        borderWidth: 1,
        borderStyle: 'solid',
        borderColor: '#C3D600',
    },
    countButtonText: {
        fontSize: 24,
        lineHeight: 30,
        color: '#C3D600'
    },
    price: {
        fontSize: 20,
        color: '#C3D600',
        fontWeight: '500',
        marginLeft: 'auto'
    },

    swipeable: {
        padding: 15,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#EF5252'
    },
    actionIcon: {
        width: 30,
        marginHorizontal: 10
    },
    rightAction: {
        alignItems: 'center',
        flex: 1,
        justifyContent: 'center',
    }
})

export default CartProductItem
